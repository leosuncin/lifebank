module lifebank-api

go 1.12

require (
	github.com/appleboy/gin-jwt/v2 v2.6.2
	github.com/bxcodec/faker v2.0.1+incompatible
	github.com/bxcodec/faker/v3 v3.1.0 // indirect
	github.com/gin-contrib/sse v0.1.0 // indirect
	github.com/gin-gonic/gin v1.4.0
	github.com/golang/protobuf v1.3.2 // indirect
	github.com/jinzhu/gorm v1.9.11
	github.com/json-iterator/go v1.1.7 // indirect
	github.com/mattn/go-isatty v0.0.10 // indirect
	github.com/ugorji/go v1.1.7 // indirect
	golang.org/x/sys v0.0.0-20191010194322-b09406accb47 // indirect
	gopkg.in/yaml.v2 v2.2.4 // indirect
)
