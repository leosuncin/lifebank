package fakes

import (
	"github.com/bxcodec/faker"
	"github.com/jinzhu/gorm"
	"lifebank-api/db/entities"
)

func MakeUsers(db *gorm.DB, quantity int) {
	for i := 1; i <= quantity; i++ {
		var u entities.User
		faker.FakeData(&u)
		u.Password = "Pa$$w0rd"
		db.Create(&u)
	}
}
