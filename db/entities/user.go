package entities

import (
	"crypto/sha512"
	"encoding/base64"

	"github.com/jinzhu/gorm"
)

type User struct {
	gorm.Model
	Username string `gorm:"unique;not null" faker:"username"`
	Password string `faker:"-"`
}

func (u *User) BeforeCreate() {
	encoder := sha512.New()
	u.Password = base64.URLEncoding.EncodeToString(encoder.Sum([]byte(base64.StdEncoding.EncodeToString([]byte(u.Username)) + base64.StdEncoding.EncodeToString([]byte(u.Password)))))
}

func (u *User) CheckPassword(password string) bool {
	encoder := sha512.New()
	return u.Password == base64.URLEncoding.EncodeToString(encoder.Sum([]byte(base64.StdEncoding.EncodeToString([]byte(u.Username)) + base64.StdEncoding.EncodeToString([]byte(password)))))
}
