package handlers

import (
	"lifebank-api/httpd/models"
	"lifebank-api/db/entities"

	"github.com/appleboy/gin-jwt/v2"
	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
)

func LoginPost(db *gorm.DB) func(c *gin.Context) (interface{}, error) {
	return func(c *gin.Context) (interface{}, error) {
		var login models.Login
		if err := c.Bind(&login); err != nil {
			return "", jwt.ErrMissingLoginValues
		}
		var user entities.User

		if err := db.First(&user, "username = ?", login.Username).Error; err != nil {
			return nil, jwt.ErrFailedAuthentication
		}

		if !user.CheckPassword(login.Password) {
			return nil, jwt.ErrFailedAuthentication
		}
		return user, nil
	}
}

