package main

import (
	"lifebank-api/db/entities"
	"lifebank-api/db/fakes"
	"lifebank-api/httpd/handlers"

	"os"
	"time"
	"net/http"

	"github.com/appleboy/gin-jwt/v2"
	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/sqlite"
)

func main() {
	var secret string = "secret_fallback"
	if value, ok := os.LookupEnv("APP_SECRET"); ok {
		secret = value
	}
	r := gin.Default()

	db, err := gorm.Open("sqlite3", "lifebank.db")
	if err != nil {
		panic(err.Error())
	}
	defer db.Close()

	// Migrate the schema
	db.AutoMigrate(&entities.User{})
	// Create 3 users
	fakes.MakeUsers(db, 3);

	authMiddleware, err := jwt.New(&jwt.GinJWTMiddleware{
		Realm: "Life Bank",
		Key: []byte(secret),
		Timeout: 30 * time.Minute,
		IdentityKey: "id",
		PayloadFunc: func(data interface{}) jwt.MapClaims {
			if u, ok := data.(entities.User); ok {
				return jwt.MapClaims{
					"id": u.Username,
				}
			}
			return jwt.MapClaims{}
		},
		IdentityHandler: func(c *gin.Context) interface{} {
			claims := jwt.ExtractClaims(c)
			return &entities.User{
				Username: claims["id"].(string),
			}
		},
		Authenticator: handlers.LoginPost(db),
		Unauthorized: func(c *gin.Context, code int, message string) {
			c.JSON(code, gin.H{
				"code":    code,
				"message": message,
			})
		},
		TokenLookup: "header: Authorization",
		LoginResponse: func(c *gin.Context, code int, token string, expire time.Time) {
			c.JSON(http.StatusOK, gin.H{
				"code":   http.StatusOK,
				"tkn":  token,
				"expire": expire.Format(time.RFC3339),
			})
		},
	})
	
	r.POST("/api/login", authMiddleware.LoginHandler)
	api := r.Group("/api")
	api.Use(authMiddleware.MiddlewareFunc())
	{
		api.GET("/products", func (c *gin.Context) {
			c.String(200, "{\"accounts\": {\"loan\": {\"id\": 1,\"name\": \"loan 1\"},\"creditCard\": {\"id\": 1,\"name\": \"credit card 1\"},\"personal\": {\"id\": 1, \"name\": \"personal 1\"}}}")
		})
	}

	r.Run()
}
